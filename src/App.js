import './App.css';
import TempTracker from './TempTracker/TempTracker';
import ErrorBoundary from './ErrorBoundary';
import { useEffect, useRef, useState } from 'react';

function App() {
  const [tempTracker] = useState(new TempTracker());
  const [inputValue, setInputValue] = useState('');
  const [inputError, setInputError] = useState();
  const [stats, setStats] = useState({
    max: 0,
    min: 0,
    mean: 0,
    mode: 0
  });
  // used to set focus on the input.
  const inputRef = useRef(null);
  
  useEffect(()=>{
    // Set input focus on first render
    inputRef.current.focus();
  },[])

  /**
   * Inserts a new temp value in TempTracker and actualize the stats values.
   *
   * @param {number} temp The integer value of the inserted temp in the range 0..1100.
   */
  const insertTemp = (temp) => {
    try{
      tempTracker.insert(temp);
      setStats({
        max: tempTracker.getMax(),
        min: tempTracker.getMin(),
        mean: tempTracker.getMean(),
        mode: tempTracker.getMode() 
      })
      setInputError(false);
    }catch(error){
      setInputError(true);
    }
  }
  
  /**
   * Main form handler
   *
   * @param {Event} e
   */
  const handleSubmit = (e) => {
    e.preventDefault()
    const tempValue = parseInt(inputValue);
    insertTemp(tempValue);
    setInputValue('');
    inputRef.current.focus();
  }

  /**
   * Input change handler, used to control it and to filter it's content to just numbers.
   *
   * @param {Event} e
   */
  const handleInputChange = (e) => {
    const value = e.target.value.replace(/\D/g, "");
    setInputValue(value);
  }
  
  return (
    <ErrorBoundary>
      <div className="App">
        {/* Input error handling  */}
        {inputError && <div className="alert">
          <span className="closebtn" onClick={()=>{setInputError(false)}}>&times;</span> 
          <strong>Danger!</strong> Incorrect temp value, please insert an integer value in the range 0..1100.
        </div>}
        
        {/* Main body */}
        <section className="App-body">
          <div className="statsgrid">
            <div className="cell cell-dark">
              <div>Max</div>
              <div>{stats.max}</div>
            </div>
            <div className="cell">
              <div>Min</div>
              <div>{stats.min}</div>
            </div>
            <div className="cell cell-dark">
              <div>Mode</div>
              <div>{stats.mode}</div>
            </div>
            <div className="cell">
              <div>Mean</div>
              <div>{stats.mean}</div>
            </div>
          </div>
          <div>
            <p/>
            Insert a temperature value in the range 0..1100:
          </div>
          <div>
            <form onSubmit={handleSubmit}>
              <input value={inputValue} className="forminput" onChange={handleInputChange} ref={inputRef}/>
              <button type="submit" className="formbtn">OK</button>
            </form>
          </div>  
        </section>
      </div>
    </ErrorBoundary>
  );
}

export default App;
