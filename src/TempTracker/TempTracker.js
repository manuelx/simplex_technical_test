/**
 * @author Manuel Marioni <manuelmarioni@gmail.com>
 * @version 1.0.0
 * 
 * Class representing a Temperature history tracker. 
 * */
class TempTracker {
  constructor(){
    // Statistical values will be calculated only once 
    // and stored when inserting a new temperature to improve the response time of get methods.
    this.max = null;
    this.min = null;
    this.mean = null;
    this.mode = null;

    // Mean auxiliar values
    this.sum = 0;
    this.totalValues = 0;

    // Mode auxiliar values
    this.repsByTemp = {};
    this.modeReps = 0;
  }

  /** 
  * Records a new temperature
  * @param {number} temp - The integer value of the inserted temp in the range 0..1100.
  */ 
  insert(temp) {
    // Check the inserted value format
    if (!Number.isInteger(temp)){
      throw new Error('Invalid argument, the inserted temperature must be an integer');
    }

    // Check value limits (the temperatures are expected in the range 0..1100)
    if (temp < 0 || temp > 1100){
      throw new Error('Invalid argument, the inserted temperature must be in the range 0..1100');
    }

    // Max & Min
    if (this.max === null || temp > this.max) this.max = temp;
    if (this.min === null || temp < this.min) this.min = temp;

    /* Mean
    * Note: With javascript number type, the mean will be only a double-precision 64-bit binary format IEEE 754 value.
    * it's recomended to use a library like bignumber.js or decimal.js to obtain a better precision in this operation.
    */
    this.sum += temp; 
    this.totalValues++;
    this.mean = this.sum / this.totalValues;

    // Mode
    this.repsByTemp[temp] !== undefined ? this.repsByTemp[temp]++ : this.repsByTemp[temp] = 1;
    
    if(this.repsByTemp[temp] > this.modeReps){
      this.mode = temp;
      this.modeReps = this.repsByTemp[temp]
    }
  }

  /**
  * Returns the highest temp we've seen so far.
  * @return {?number} Integer value or null when there is no temps inserted.
  */
  getMax() {
    return this.max;
  }

  /**
  * Returns the lowest temp we've seen so far.
  * @return {?number} Integer value or null when there is no temps inserted.
  */
  getMin(){
    return this.min;
  }

  /**
  * Returns the arithmetic mean of all temps we've seen so far.
  * @return {?number} Float number or null when there is no temps inserted.
  */
  getMean(){
    return this.mean;
  }

  /**
  * Returns a mode of all temps we've seen so far. The value that appears most on the inserted temperatures.
  * @return {?number} Integer value or null when there is no temps inserted.
  */
  getMode(){
    return this.mode;
  }
}

export default TempTracker;