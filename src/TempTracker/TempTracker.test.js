import TempTracker from './TempTracker';

test('TempTracker normal functioning', () => {
    const tempTracker = new TempTracker();
    tempTracker.insert(1);
    expect(tempTracker.getMax()).toEqual(1);
    tempTracker.insert(2);
    expect(tempTracker.getMax()).toEqual(2);
    expect(tempTracker.getMin()).toEqual(1);
    expect(tempTracker.getMean()).toEqual(1.5);
    tempTracker.insert(2);
    expect(tempTracker.getMode()).toEqual(2);
  });

  test('TempTracker Error handling', () => {
    const tempTracker = new TempTracker();
    
    // Test insert() call with no value.
    const errorThrowOnNoValue = () => {
        tempTracker.insert();
    };
    expect(errorThrowOnNoValue).toThrow(Error);

    // Test insert() call with a string value.
    const errorThrowOnString = () => {
        tempTracker.insert('');
    };
    expect(errorThrowOnString).toThrow(Error);

    // Test insert() call with a value higher than 1100.
    const errorThrowOnHighValue = () => {
        tempTracker.insert(1200);
    };
    expect(errorThrowOnHighValue).toThrow(Error);
  });